import time
from collections import defaultdict
import datetime
from kafka import KafkaConsumer
import pickle
import os
import sys

from typing import Dict

sys.path.insert(0, f'{os.path.expanduser("~/")}Desktop/etl_work/kafka_airflow_util')
from native_utils.other import get_run_as_user


# class StatsKafkaCons_jobs(ABC):
class StatsKafkaCons_jobs:

    def __init__(self, consumer: KafkaConsumer,
                 max_message_recieved: int,
                 message_cnt_to_verbose: int,
                 poll_timeout: int,
                 topic_name: str = "my_topic",
                 group_id: str or int = None,
                 bootstrap_servers: list = [],
                 save_path: str = f"/data/users/{get_run_as_user()}/kafka_datas"
                 ):
        """ Класс для работы с топиками сообщений из продюсеров брокера Kafka

        :param consumer: объект консюмера (получатель данных)
        :param save_path: путь сохранения данных
        :param max_message_recieved: максимальный порог получаемых сообщений
        :param message_cnt_to_verbose: 
        :param poll_timeout: таймаут запроса в продюссер (источник данных)
        """
        #  объект и креды брокера: 
        self.kafka_consumer = consumer(topic_name, 
                                       group_id=group_id, 
                                       bootstrap_servers=bootstrap_servers)
        
        self.save_path = save_path
        self.max_message_recieved = max_message_recieved
        self.message_cnt_to_verbose = message_cnt_to_verbose
        self.poll_timeout = poll_timeout
        self.recieved_messages = defaultdict(list)
        
    def build_messages(self,
                       max_message_received: int = 5000,
                       message_cnt_to_verbose: int = 100, 
                       timeout_ms=60 * 100) -> dict:
        """ Метод определения стабильного тайм-аута для определения постоянной нагрузки
        
        :param max_message_received: предельное значение кол-ва сообщений за итерацию
            прослушивания, defaults to 5000
        :param message_cnt_to_verbose: явное указание на требуемое кол-во сообщений, 
            defaults to 100
        :param timeout_ms: тайм-аут между запросами, defaults to 60*100
        :return: словарь сообщения
        """
        received_messages_cnt = 0
        received_timestamp = datetime.datetime.now()
        # Хранит все сообщения, сгруппированные по типу события
        start = time.time()
        while True:
            received_messages_cnt = self._accumulate_messages_poll(self.recieved_messages,
                                                                  max_message_received, 
                                                                  received_messages_cnt,
                                                                  message_cnt_to_verbose)
            elapsed_ms = (time.time() - start) * 1000
            remaining = timeout_ms - elapsed_ms
            if remaining <= 0 or received_messages_cnt >= max_message_received:
                return self.recieved_messages

    def _accumulate_messages_poll(self,
                                 messages_dict: dict,
                                 max_message_received: int = 5000,
                                 accepted_msg_count: int = 0,
                                 message_cnt_to_verbose: int = 100) -> int:
        """ Метод для получения сообщений топика из 
        очереди и их подсчета

        :param messages_dict: топик (словарь сообщений)
        :param max_message_received: максимальное количество возвращаемых 
            записей из топика, defaults to 5000
        :param accepted_msg_count: принимаемые сообщения (кол-во), defaults to 0
        :param message_cnt_to_verbose: указание таргетного кол-ва (предела), defaults to 100
        :return: _description_
        """
        poll: dict = self.kafka_consumer.poll(timeout_ms=self.poll_timeout,
                                              max_records=max_message_received)
        received_msg_count: int = accepted_msg_count
        for tp, messages in poll.items():
            for message in messages:
                received_msg_count += 1
                if not (received_msg_count % message_cnt_to_verbose):
                    print(f"Счетчик полученных сообщений: ({received_msg_count})")
                payload, event_type = self._message_handler(message)
                messages_dict[event_type].append(payload)
        return received_msg_count

    def _message_saver(self, recieved_messages: Dict[str, list]) -> Dict[str, str]:
        """ Метод сохранения сообщения

        :param recieved_messages: словарь, полученных сообщений, в сырой структуре
        :return: словарь, полученных сообщений, обработанной структуры
        """
        
        paths_to_messages = {}
        for event_type, payload in recieved_messages.items():
            event_type_folder = os.path.join(self.save_path, event_type)
            
            if not os.path.exists(event_type_folder):
                os.mkdir(event_type_folder)
            
            filename = f"{event_type}_{datetime.datetime.now().strftime('%Y-%m-%d_%H:%M:%S')}"
            
            filepath = os.path.join(event_type_folder, filename)
            with open(filepath, 'wb') as event_type_messages_pickle:
                pickle.dump(payload, event_type_messages_pickle)
            paths_to_messages[event_type] = filepath
        return paths_to_messages

    def read_handle_and_save_message(self):
        """ Основной метод для получения сообщения и сохранения сообщений
            по пути из save_path
        
        :return:
        """
        try:
            recieved_messages: Dict[str, list] = self.build_messages(max_message_received=self.max_message_recieved,
                                                                     message_cnt_to_verbose=self.message_cnt_to_verbose,
                                                                     timeout_ms=self.poll_timeout)
            self.kafka_consumer.close()
        except Exception as err:
            return False
        finally:
            self.kafka_consumer.close()

        path_to_read = self._message_saver(recieved_messages)
        return path_to_read


# TODO: Remove from base consumer
def events_messages_reader(events_pickle_path: str) -> list:
    """
    Преобразует в бинарный pickle событий
    :param events_pickle_path: путь к pickle с разными событиями
    :return: список сообщений
    """
    employee_save_messages = []
    if events_pickle_path:
        with open(events_pickle_path, 'rb') as employee_save_messages_pickle:
            employee_save_messages = pickle.load(employee_save_messages_pickle)
    return employee_save_messages
