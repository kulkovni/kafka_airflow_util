from os import getuid
from os.path import basename
from pwd import getpwuid


def get_run_as_user():
    return getpwuid(getuid())[0]


def get_file_name(file_name):
    fn = str(basename(file_name))
    return fn[:fn.rfind('.')]